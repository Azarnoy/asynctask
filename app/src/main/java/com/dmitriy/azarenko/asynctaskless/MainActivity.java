package com.dmitriy.azarenko.asynctaskless;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    MyTask myTask;
    TextView text;
    Button startButton;
    ProgressBar progress;
    ProgressBar progress2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.textView);
        startButton = (Button) findViewById(R.id.button);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress2 = (ProgressBar) findViewById(R.id.progressBar2);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTask = new MyTask();
                myTask.execute();

            }
        });








    }

    class MyTask extends AsyncTask<Void, Integer, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            text.setText("Starting...");
            startButton.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

              int counter = 0;
            for (int i = 0; i < 20;i++){
                publishProgress(counter++);

                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            text.setText("progress " + values[0]);
            progress2.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            text.setText("Finish");
            startButton.setVisibility(View.VISIBLE);
            progress2.setProgress(0);
        }
    }
}
